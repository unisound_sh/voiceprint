package com.unisound.vpr.demo;

/**
 * @author songhonglei
 * @email songhonglei@unisound.com
 * @date 2022/4/15 17:37:46
 */
public class VprConstants {
    /**
     * 创建声纹组
     */
    public static final String CREATE_GROUP_ENDPOINT = "/vpr/v1/createGroup";
    /**
     * 创建声纹
     */
    public static final String CREATE_FEATURE_ENDPOINT = "/vpr/v1/createFeature";
    /**
     * 声纹确认 1:1
     */
    public static final String CONFIRM_FEATURE_ENDPOINT = "/vpr/v1/confirmFeature";
    /**
     * 声纹确认 1:N 组内所有声纹作对比
     */
    public static final String IDENTIFY_FEATURE_BY_GROUP_ID_ENDPOINT = "/vpr/v1/identifyFeatureByGroupId";
    /**
     * 声纹确认 1:N  通过一个音频和多个已注册的声纹做对比，提供不同组内的多个声纹id
     */
    public static final String IDENTIFY_FEATURE_BY_IDS_ENDPOINT = "/vpr/v1/identifyFeatureByIds";
    /**
     * 修改声纹
     */
    public static final String UPDATE_FEATURE_BY_ID_ENDPOINT = "/vpr/v1/updateFeatureById";
    /**
     * 删除声纹
     */
    public static final String DEL_FEATURE_BY_ID_ENDPOINT = "/vpr/v1/delFeatureById";
    /**
     * 删除声纹组
     */
    public static final String DEL_GROUP_BY_ID_ENDPOINT = "/vpr/v1/delGroupById";
    /**
     * 查询组内声纹列表
     */
    public static final String FIND_FEATURE_LIST_BY_GROUP_ID_ENDPOINT = "/vpr/v1/findFeatureListByGroupId";
}
