package com.unisound.vpr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author YZS
 */
@SpringBootApplication
public class VprJavaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(VprJavaDemoApplication.class, args);
    }

}
