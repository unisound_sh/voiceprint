# Demo说明
此demo是基于Umi + ant design实现，详细信息请参考 [Umi](https://umijs.org/zh-CN)

# 快速上手
  ## 1.安装Node.js
  可以到[NodeJs](https://nodejs.org/en/download/)下载安装
  ## 2.安装依赖
  在命令行中进入到此目录下执行
  ```bash
    $ npm install
  ```
  注：由于node下载第三方依赖包是从国外服务器下载，下载的速度可能非常的缓慢且有可能会出现异常，所以为了提高效率，我们还是把npm的镜像源替换成淘宝的镜像源或者使用cnpm，具体操作方法可参考 [npm更换淘宝镜像](https://www.cnblogs.com/cythia/p/10985080.html)。

  ## 3.开始调试
  ```bash
    $ npm run start
  ```
    
# 其他说明
  声纹识别的主要代码在/src/pages/layout/feature目录下
  由于浏览器限制跨域请求，调试时接口地址一栏直接填写请求地址会被浏览器阻拦，所以需要配合webpack-server的反向代理功能进行调试。可以在.umirc.ts文件中配置，具体可参考.umirc.ts中proxy 项下的注释说明。配置好后在接口地址一栏中填写 /proxy 即可。


