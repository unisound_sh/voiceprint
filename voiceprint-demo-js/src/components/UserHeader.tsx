import { Layout, Menu } from 'antd';
import React from 'react';
import { Link } from 'umi';
import styles from './userheader.less';
const { Header } = Layout;

export default () => {
  return (
    <Header className={styles.header}>
      <div className={styles.headerContent}>
        <div className={styles.logo}>
          <a href="/">
            <img
              src={require('@/assets/header-logo.png')}
              className={styles.headerLogo}
            />
            <img
              src={require('@/assets/icon_beta.png')}
              style={{ marginRight: 26 }}
            />
          </a>
        </div>
        <Menu
          mode="horizontal"
          className={styles.pcMenu}
          selectedKeys={['']}
          selectable={false}
        >

          <Menu.Item key="feature">
            <Link to="/feature">声纹识别</Link>
          </Menu.Item>
        </Menu>
      </div>
    </Header>
  );
};
