import UserHeader from '@/components/UserHeader';
import { Layout } from 'antd';
import React, { FC } from 'react';
import BaseSetting from './BaseSetting';
import styles from './index.less';
interface IndexProps {
  children?: React.ReactNode;
}
const { Content } = Layout;

const Index: FC<IndexProps> = ({ children }) => {
  return (
    <Layout className={styles.page}>
      <UserHeader />
      <Content className={styles.content}>
        <BaseSetting />
        {children}
      </Content>
    </Layout>
  );
};
export default Index;
