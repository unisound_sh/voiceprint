import { Col, Input, Row } from 'antd';
import React, { FC } from 'react';
import { useModel } from 'umi';
import cstyle from './common.less';

interface IndexProps {
  children?: React.ReactNode;
}

const Index: FC<IndexProps> = ({ children }: any) => {
  let {
    appkey,
    setAppkey,
    secret,
    setSecret,
    path,
    setPath,
    count,
    setCount,
  } = useModel('useSettingsModel');
  return (
    <div className={cstyle.bigBlock} id="test">
      <div className={cstyle.title}>基础设置</div>
      <Row className={cstyle.row}>
        <Col span={6} className={cstyle.label}>
          AppKey
        </Col>
        <Col span={16}>
          <Input
            value={appkey}
            onChange={e => {
              setAppkey(e.target.value);
            }}
          />
        </Col>
      </Row>
      <Row className={cstyle.row}>
        <Col span={6} className={cstyle.label}>
          AppSecret
        </Col>
        <Col span={16}>
          <Input
            value={secret}
            onChange={e => {
              setSecret(e.target.value);
            }}
          />
        </Col>
      </Row>
      <Row className={cstyle.row}>
        <Col span={6} className={cstyle.label}>
          接口地址
        </Col>
        <Col span={16}>
          <Input
            value={path}
            onChange={e => {
              setPath(e.target.value);
            }}
          />
        </Col>
      </Row>
      {/* <Row className={cstyle.row}>
        <Col span={6} className={cstyle.label}>
          保留文本句数
        </Col>
        <Col span={16}>
          <InputNumber
            value={count}
            min={1}
            onChange={v => {
              setCount(v);
            }}
          />
        </Col>
      </Row> */}
    </div>
  );
};
export default Index;
