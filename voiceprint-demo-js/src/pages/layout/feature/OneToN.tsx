import * as React from 'react';
import Recorder from './Recorder';
import cstyle from '../common.less';
import style from './test.less';
import api from './api'
import { useModel } from 'umi';
import { message } from 'antd';
const { createFeature, identifyFeatureByIds } = api
export default ({ afterCreateFeature = () => { } }) => {
  const config = useModel('useSettingsModel');
  const featureId = React.useRef(['', '', '', ''])
  const [result, setResult] = React.useState<any[]>([])
  return <React.Fragment>
    <div className={style.box}>
      <div className={cstyle.subTitle}>原始语音（≤60秒）</div>
      <div className={style.borderBox}>
        <div className={style.row}>
          <Recorder onEnd={(audio) => {
            createFeature(config, audio).then((res: any) => {
              console.log(res)
              if (res.code === 0) {
                afterCreateFeature()
                featureId.current[0] = res.data.featureId
              } else
                message.error('创建声纹失败！' + res.msg)
            })
          }} desc="语音A" />
          <Recorder onEnd={(audio) => {
            createFeature(config, audio).then((res: any) => {
              console.log(res)
              if (res.code === 0) {
                afterCreateFeature()
                featureId.current[1] = res.data.featureId
              } else
                message.error('创建声纹失败！' + res.msg)
            })
          }} desc="语音B" />
        </div>
        <div className={style.row}>
          <Recorder onEnd={(audio) => {
            createFeature(config, audio).then((res: any) => {
              console.log(res)
              if (res.code === 0) {
                afterCreateFeature()
                featureId.current[2] = res.data.featureId
              } else
                message.error('创建声纹失败！' + res.msg)
            })
          }} desc="语音C" />
          <Recorder onEnd={(audio) => {
            createFeature(config, audio).then((res: any) => {
              console.log(res)
              if (res.code === 0) {
                afterCreateFeature()
                featureId.current[3] = res.data.featureId
              } else
                message.error('创建声纹失败！' + res.msg)
            })
          }} desc="语音D" />
        </div>
      </div>
    </div>
    <div className={style.box}>
      <div className={cstyle.subTitle}>对比语音（≤60秒）</div>
      <div className={style.borderBox}>
        <Recorder onEnd={(audio) => {
          if (!featureId.current) {
            message.error('还没有创建声纹！')
            return
          }
          identifyFeatureByIds(config, audio, featureId.current).then((res: any) => {
            console.log(res)
            if (res.code === 0) {
              console.log(res)
              setResult(res.data)
            } else {
              message.error('创建声纹失败！' + res.msg)
            }

          })
        }} />
      </div>
    </div>
    <div className={style.box}>
      <div className={cstyle.subTitle}>对比结果</div>
      <div className={style.borderBox}>
        {result.map((t, index) => {
          return (<div className={style.desc}>
            与语音{String.fromCharCode(65 + index)} 为同一个人声音的概率：{Math.round(+t.score * 100) / 100}%
          </div>)
        })}
      </div>
    </div>
  </React.Fragment>

}

