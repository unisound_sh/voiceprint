import { sha256 } from 'js-sha256';
import { guid8 } from '@/utils/guid';
import Axios from 'axios';

function BolbToBase64(data: Blob) {
  return new Promise<string>(reslove => {
    var reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onload = function (e: any) {
      // 去掉"data:application/octet-stream;base64,"
      reslove(e.target.result.substr(37));
    };
  });
}

function getSign(
  appkey: string,
  timestamp: number | string,
  secret: string,
  nonce: string,
) {
  return sha256(`${appkey}${timestamp}${secret}${nonce}`).toUpperCase();
}
interface Config {
  path: string;
  appkey: string;
  secret: string;
  featureId?: number | string;
}
const groupId = 'vpr-python-demo';
let currentGroupId = '';
const featureInfo = '我的声纹测试';
const createGroupEndPoint = '/vpr/v1/createGroup',
  createFeatureEndPoint = '/vpr/v1/createFeature',
  confirmFeatureEndPoint = '/vpr/v1/confirmFeature',
  identifyFeatureByIdsEndPoint = '/vpr/v1/identifyFeatureByIds',
  findFeatureListByGroupId = '/vpr/v1/findFeatureListByGroupId',
  delFeatureById = '/vpr/v1/delFeatureById',
  delGroupById = '/vpr/v1/delGroupById',
  updateFeatureById = '/vpr/v1/updateFeatureById';


export default {
  createGroup(config: Config) {
    return new Promise((reslove, reject) => {
      const { path, appkey, secret } = config;
      const timestamp = Date.now();
      const nonce = guid8();
      const sign = getSign(appkey, timestamp, secret, nonce);
      currentGroupId = groupId + nonce;
      const create_feature_param = {
        appkey: appkey,
        timestamp: timestamp,
        nonce: nonce,
        sign: sign,
        groupId: currentGroupId,
        groupInfo: 'groupInfo',
      };
      console.log('create_feature_param', create_feature_param);
      console.log('host', path, createGroupEndPoint);
      Axios.post(path + createGroupEndPoint, create_feature_param).then(
        res => {
          reslove(res.data);
        },
      );;

    });
  },
  findFeatureListByGroupId(config: Config) {
    return new Promise((reslove, reject) => {
      const { path, appkey, secret } = config;
      const timestamp = Date.now();
      const nonce = guid8();
      const sign = getSign(appkey, timestamp, secret, nonce);
      const create_feature_param = {
        appkey: appkey,
        timestamp: timestamp,
        nonce: nonce,
        sign: sign,
        groupId: currentGroupId,
      };
      console.log('create_feature_param', create_feature_param);
      console.log('host', path, findFeatureListByGroupId);
      Axios.post(path + findFeatureListByGroupId, create_feature_param).then(
        res => {
          reslove(res.data);
        },
      );

    });
  },
  delGroupById(config: Config) {
    return new Promise((reslove, reject) => {
      const { path, appkey, secret } = config;
      const timestamp = Date.now();
      const nonce = guid8();
      const sign = getSign(appkey, timestamp, secret, nonce);
      const create_feature_param = {
        appkey: appkey,
        timestamp: timestamp,
        nonce: nonce,
        sign: sign,
        groupId: currentGroupId,
      };
      Axios.post(path + delGroupById, create_feature_param).then(
        res => {
          reslove(res.data);
        },
      );

    });
  },
  createFeature(config: Config, data: Blob) {
    return new Promise((reslove, reject) => {
      BolbToBase64(data).then((base64Data: string) => {
        const { path, appkey, secret } = config;
        const timestamp = Date.now();
        const nonce = guid8();
        const sign = getSign(appkey, timestamp, secret, nonce);
        const create_feature_param = {
          appkey: appkey,
          timestamp: timestamp,
          nonce: nonce,
          sign: sign,
          groupId: currentGroupId,
          featureInfo: featureInfo,
          audioData: base64Data,
          audioSampleRate: 8000,
          audioFormat: 'pcm',
        };
        console.log('create_feature_param', create_feature_param);
        console.log('host', path, createFeatureEndPoint);
        Axios.post(path + createFeatureEndPoint, create_feature_param).then(
          res => {
            console.log('create feature result', res.data);
            reslove(res.data);
          },
        );
      });
    });
  },
  updateFeature(config: Config, data: Blob) {
    return new Promise((reslove, reject) => {
      BolbToBase64(data).then((base64Data: string) => {
        const { path, appkey, secret, featureId } = config;
        const timestamp = Date.now();
        const nonce = guid8();
        const sign = getSign(appkey, timestamp, secret, nonce);
        const create_feature_param = {
          appkey: appkey,
          timestamp: timestamp,
          nonce: nonce,
          sign: sign,
          groupId: currentGroupId,
          featureInfo: featureInfo,
          audioData: base64Data,
          audioSampleRate: 8000,
          audioFormat: 'pcm',
          featureId
        };

        Axios.post(path + updateFeatureById, create_feature_param).then(
          res => {
            console.log('create feature result', res.data);
            reslove(res.data);
          },
        );
      });
    });
  },
  delFeatureById(config: Config, featureId: number | string) {
    return new Promise(reslove => {
      const { path, appkey, secret } = config;
      const timestamp = Date.now();
      const nonce = guid8();
      const sign = getSign(appkey, timestamp, secret, nonce);
      const create_feature_param = {
        appkey: appkey,
        timestamp: timestamp,
        nonce: nonce,
        sign: sign,
        groupId: currentGroupId,
        featureId: featureId,
      };
      Axios.post(path + delFeatureById, create_feature_param).then(
        res => {
          reslove(res.data);
        },
      );;
    })
  },
  confirmFeature(config: Config, data: Blob, featureId: number | string) {
    return new Promise((reslove, reject) => {
      BolbToBase64(data).then((base64Data: string) => {
        const { path, appkey, secret } = config;
        const timestamp = Date.now();
        const nonce = guid8();
        const sign = getSign(appkey, timestamp, secret, nonce);
        const create_feature_param = {
          appkey: appkey,
          timestamp: timestamp,
          nonce: nonce,
          sign: sign,
          groupId: currentGroupId,
          featureId: featureId,
          audioData: base64Data,
          audioSampleRate: 8000,
          audioFormat: 'pcm',
        };
        console.log('confirmFeatureEndPoint params', create_feature_param);
        Axios.post(path + confirmFeatureEndPoint, create_feature_param).then(
          res => {
            console.log('confirmFeatureEndPoint result', res);
            reslove(res.data);
          },
        );
      });
    });
  },
  identifyFeatureByIds(
    config: Config,
    data: Blob,
    featureIds: number[] | string[],
  ) {
    return new Promise((reslove, reject) => {
      BolbToBase64(data).then((base64Data: string) => {
        const { path, appkey, secret } = config;
        const timestamp = Date.now();
        const nonce = guid8();
        const sign = getSign(appkey, timestamp, secret, nonce);
        const featureList = featureIds
          .map(t => {
            if (t) return { groupId: currentGroupId, featureId: t };
          })
          .filter(t => !!t);
        const create_feature_param = {
          appkey: appkey,
          timestamp: timestamp,
          nonce: nonce,
          sign: sign,
          groupId: currentGroupId,
          featureList: featureList,
          topN: featureList.length,
          audioData: base64Data,
          audioSampleRate: 8000,
          audioFormat: 'pcm',
        };
        console.log(
          'identifyFeatureByIdsEndPoint params',
          create_feature_param,
        );
        Axios.post(
          path + identifyFeatureByIdsEndPoint,
          create_feature_param,
        ).then(res => {
          console.log('identifyFeatureByIdsEndPoint result', res);
          reslove(res.data);
        });
      });
    });
  },
};
