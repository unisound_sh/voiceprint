import { Button, message } from 'antd';
import * as React from 'react';
import { useRef, useState } from 'react';
import { useModel } from 'umi';
import Recorder from '@/utils/Recorder';
import style from './test.less';

export default ({ onEnd = (audio: any) => { }, desc = "" }) => {
  const [recording, setRecording] = useState(false);
  const [showReplay, setShowReplay] = useState(false)
  const { appkey, secret, path } = useModel('useSettingsModel');
  const [time, setTime] = useState(0);
  let ctime = time;
  const interalHandler = useRef<any>()
  const recorder = useRef<any>()
  const audio = useRef<any>()
  function startRecording() {
    if (!path) {
      message.error('请填写体验接口 ');
      return;
    }
    if (!appkey) {
      message.error('请填写AppKey ');
      return;
    }
    if (!secret) {
      message.error('请填写AppSecret ');
      return;
    }
    setRecording(true);

    interalHandler.current = setInterval(() => {
      ctime = ctime + 1;
      if (ctime > 60) {
        endRecording();
      }
      setTime(ctime);
    }, 1000);
    doRecording();
  }
  function endRecording() {
    if (recorder.current && audio.current) {
      const audioBuffer = recorder.current.getPCMBlob()
      audio.current.src = window.URL.createObjectURL(audioBuffer)
      onEnd(audioBuffer)
    }

    clearInterval(interalHandler.current);
    setRecording(false);
    setTime(0);
    ctime = 0;
    stopRecording();
    setShowReplay(true)
  }

  function doRecording() {
    recorder.current = new Recorder(undefined, { sampleRate: 8000 });
    recorder.current.ready().then(
      () => {
        recorder.current.start();
      },
      (err: any) => {
        console.log(err)
        message.warn('录音启动失败！');
        endRecording()
      },
    );
  }
  function stopRecording() {
    if (!recorder.current) return;
    recorder.current.stop();
  }
  const timer = `0:${time > 9 ? time : '0' + time}`;
  return <div className={style.micbox}>
    <div
      onClick={() => {
        if (!recording) {
          startRecording();
        } else {
          endRecording();
        }
      }}
      className={recording ? style.imgRecording : style.img}
    >
      {recording && (
        <div className={style.imgtext}>
          <img src={require('@/assets/icon_pause.png')} height="33" />
          <div>
            <span style={{ color: '#FFB700' }}>{timer}</span>
            <span>/1:00</span>
          </div>
        </div>
      )}
    </div>
    <div className={style.desc}>{desc}</div>
    {showReplay && <Button
      type='primary'
      onClick={() => {
        audio.current.play();
      }}
    >
      回放
    </Button>}
    <audio ref={audio} />
  </div>

}
