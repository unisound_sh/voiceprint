import * as React from 'react';
import Recorder from './Recorder';
import cstyle from '../common.less';
import style from './test.less';
import api from './api'
import { useModel } from 'umi';
import { message } from 'antd';
const { createFeature, confirmFeature } = api
export default ({ afterCreateFeature = () => { } }) => {
  const config = useModel('useSettingsModel');
  const featureId = React.useRef<string>()
  const [result, setResult] = React.useState<number | null>()
  return <React.Fragment>
    <div className={style.box}>
      <div className={cstyle.subTitle}>原始语音（≤60秒）</div>
      <div className={style.borderBox}>
        <Recorder onEnd={(audio) => {
          createFeature(config, audio).then((res: any) => {
            console.log(res)
            if (res.code === 0) {
              afterCreateFeature()
              featureId.current = res.data.featureId
            } else
              message.error('创建声纹失败！' + res.msg)
          })
        }} />
      </div>
    </div>
    <div className={style.box}>
      <div className={cstyle.subTitle}>对比语音（≤60秒）</div>
      <div className={style.borderBox}>
        <Recorder onEnd={(audio) => {
          if (!featureId.current) {
            message.error('还没有创建声纹！')
            return
          }
          confirmFeature(config, audio, featureId.current).then((res: any) => {
            console.log(res)
            if (res.code === 0) {
              console.log(res)
              setResult(res.data.score)
            } else {
              message.error('创建声纹失败！' + res.msg)
            }

          })
        }} />
      </div>
    </div>
    <div className={style.box}>
      <div className={cstyle.subTitle}>对比结果</div>
      <div className={style.borderBox}>
        {result && <div className={style.desc}>
          为同一个人声音的概率：{Math.round(+result * 100) / 100}%
        </div>}
      </div>
    </div>
  </React.Fragment>

}
