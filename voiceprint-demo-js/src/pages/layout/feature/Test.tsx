
import { Button, message, Modal, Space, Table, Tabs } from 'antd';
import { sha256 } from 'js-sha256';
import React, { useEffect, useState } from 'react';
import { useModel } from 'umi';
import cstyle from '../common.less';
import style from './test.less';
import OneToOne from './OneToOne';
import OneToN from './OneToN';
import Recorder from './Recorder';

const { TabPane } = Tabs;
import api from './api';
import { set } from 'mobx';
export default () => {
  const config = useModel('useSettingsModel');
  const [groupId, setGroupId] = useState();
  const [featureId, setFeatureId] = useState();
  const [featureList, setFeatureList] = useState([]);
  const [openUpdate, setOpenUpdate] = useState(false);
  useEffect(() => {
    // api.createGroup(config);
  }, []);
  function findFeatureListByGroupId() {
    api.findFeatureListByGroupId(config).then((res: any) => {
      setFeatureList(res.data);
    });
  }
  return (
    <div
      style={{
        backgroundColor: '#f7f9fb',
        paddingBottom: 40,
      }}
      className={cstyle.bigBlock}
      id="test"
    >
      <div className={cstyle.title}>声纹识别</div>
      <div>声纹组：{groupId}{groupId ? (<Button onClick={() => {
        api.delGroupById(config).then((res: any) => {
          setGroupId(undefined)
        })
      }}>删除声纹组</Button>) : (<Button onClick={() => {
        api.createGroup(config).then((res: any) => {
          setGroupId(res.data.groupId)
        })
      }}>创建声纹组</Button>)}</div>
      {groupId && <div>
        <Table
          columns={[
            { title: '声纹ID', dataIndex: 'featureId' },
            { title: '声纹信息', dataIndex: 'featureInfo' },
            {
              title: '操作', dataIndex: 'action', render: (_, record: any) => (<Space>
                <Button onClick={() => {
                  setFeatureId(record.featureId)
                  setOpenUpdate(true)
                }}>更新声纹</Button>
                <Button onClick={() => {
                  api.delFeatureById(config, record.featureId).then(() => {
                    findFeatureListByGroupId();
                  });
                }}>删除声纹</Button>
              </Space>)
            }]}
          dataSource={featureList}
          rowKey="featureId"
          pagination={false}
        />
      </div>}
      {groupId && <Tabs defaultActiveKey="1">
        <TabPane tab="声纹1:1" key="1">
          <div className={style.boxContainer}>
            <OneToOne afterCreateFeature={findFeatureListByGroupId} />
          </div>
        </TabPane>
        <TabPane tab="声纹1:N" key="2">
          <div className={style.boxContainer}>
            <OneToN afterCreateFeature={findFeatureListByGroupId} />
          </div>
        </TabPane>
      </Tabs>}
      <Modal title="更新声纹" visible={openUpdate} footer={null} onCancel={() => setOpenUpdate(false)}>
        <div className={style.borderBox}>
          <Recorder onEnd={(audio) => {
            api.updateFeature({ ...config, featureId }, audio).then((res: any) => {
              if (res.code === 0) {
                findFeatureListByGroupId()
                setOpenUpdate(false)
                message.success('更新声纹成功！')
              } else
                message.error('更新声纹失败！' + res.msg)
            })
          }} />
        </div>
      </Modal>
    </div>
  );
};
