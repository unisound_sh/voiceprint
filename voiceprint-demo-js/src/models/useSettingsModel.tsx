import { useEffect, useState } from 'react';

export default function useDevicesModel() {
  const [appkey, setAppkey] = useState(sessionStorage.getItem('appkey') || '');
  const [secret, setSecret] = useState(sessionStorage.getItem('secret') || '');
  const [path, setPath] = useState(sessionStorage.getItem('path') || '');
  useEffect(() => {
    sessionStorage.setItem('appkey', appkey)
    sessionStorage.setItem('secret', secret)
    sessionStorage.setItem('path', path)
  }, [appkey, secret, path])
  return {
    appkey,
    setAppkey,
    secret,
    setSecret,
    path,
    setPath,
  };
}
