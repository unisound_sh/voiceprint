import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  hash: true,
  publicPath: './',
  history: { type: 'memory' },
  routes: [
    {
      path: '/',
      component: '@/pages/layout/index',
      routes: [
        {
          path: '/feature',
          component: '@/pages/layout/feature/Test',
          title: '声纹识别-云知声AI开放平台',
        },
        {
          path: '/',
          redirect: 'feature',
        },
      ],
    },
  ],
  proxy: {
    '/proxy': {        // 接口地址一栏与此处相同
      target: 'https://ai-vpr.hivoice.cn', // host 比如  https://ai-vpr.hivoice.cn
      changeOrigin: true,
      pathRewrite: { '^/proxy': '' },
    },
  },
  theme: {
    'primary-color': '#1564FF',
    'border-radius-base': '4px',
  },
});
